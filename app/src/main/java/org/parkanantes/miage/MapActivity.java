package org.parkanantes.miage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

import org.parkanantes.miage.event.EventBusManager;
import org.parkanantes.miage.event.ListParkingsEvent;
import org.parkanantes.miage.model.database.Parking;
import org.parkanantes.miage.services.DisponibilitesSearchService;
import org.parkanantes.miage.tools.ApiConstants;
import org.parkanantes.miage.tools.exception.ExceptionMessageManager;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Activité pour afficher une map Google Maps contenant les parkings
 */
public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mActiveGoogleMap;
    private Map<String, Parking> mMarkersToParkings = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Get map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        DisponibilitesSearchService.INSTANCE.searchDisponibilites();
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    private float getMarkerColor(Parking parking) {
        float markerColor = BitmapDescriptorFactory.HUE_ORANGE  ;
        if (ApiConstants.OUVERT.equals(parking.getParkingStatut())) {
            markerColor = BitmapDescriptorFactory.HUE_GREEN;
        } else if (ApiConstants.FERME.equals(parking.getParkingStatut())
                || ApiConstants.COMPLET.equals(parking.getParkingStatut())) {
            markerColor = BitmapDescriptorFactory.HUE_RED;
        } else if (ApiConstants.ABONNEES.equals(parking.getParkingStatut())) {
            markerColor = BitmapDescriptorFactory.HUE_BLUE;
        }
        return markerColor;
    }

    @Subscribe
    public void allParkingDispoResult( final ListParkingsEvent parkings) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Check that map is ready
                if (mActiveGoogleMap != null) {
                    // Update map's markers
                    mActiveGoogleMap.clear();
                    mMarkersToParkings.clear();

                    LatLngBounds.Builder cameraBounds = LatLngBounds.builder();
                    for (Parking parking : parkings.getParkings()) {

                        MarkerOptions markerOptions = new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(parking.getLatitude()), Double.parseDouble(parking.getLongitude())))
                                .title(parking.getNom())
                                .snippet(parking.getParkingStatut() + "\n"
                                        + parking.getAdresse() + "\n"
                                        + parking.getCodePostal() + "\n"
                                        + parking.getCommune() + "\n"
                                        + getResources().getString(R.string.places_libres)
                                        + parking.getPlacesDisponibles())
                                .icon(BitmapDescriptorFactory.defaultMarker(getMarkerColor(parking)));

                        cameraBounds.include(markerOptions.getPosition());

                        Marker marker = mActiveGoogleMap.addMarker(markerOptions);
                        mMarkersToParkings.put(marker.getId(), parking);
                    }

                    if (!parkings.getParkings().isEmpty()) {
                        int width = getResources().getDisplayMetrics().widthPixels;
                        int height = getResources().getDisplayMetrics().heightPixels;
                        int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

                        mActiveGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(cameraBounds.build(), width, height, padding));
                    }
                }
            }
        });
    }

    @Subscribe
    public void errorOccured(final Exception error) {
        ExceptionMessageManager.showException(error, this);
        Toast.makeText(this, R.string.cannot_show_map_exception_message,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mActiveGoogleMap = googleMap;
        mActiveGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mActiveGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mActiveGoogleMap.setInfoWindowAdapter(this.adapterMarker());

        mActiveGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Parking associatedParking = mMarkersToParkings.get(marker.getId());
                if (associatedParking != null) {
                    Intent parkingDetailIntent = new Intent(MapActivity.this, DetailActivity.class);
                    parkingDetailIntent.putExtra("id_parking", associatedParking.getIdParking());
                    startActivity(parkingDetailIntent);
                }
            }
        });
    }

    /**
     * Permet de créer un adapter specifique qui rend possible d'afficher plus de text (multi ligne) pour les snippet
     * @return
     */
    private GoogleMap.InfoWindowAdapter adapterMarker(){
        return new GoogleMap.InfoWindowAdapter() {

            //gestion de la fenetre
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            //gestion du contenu
            @Override
            public View getInfoContents(Marker marker) {

                Context context = getApplicationContext();

                LinearLayout info = new LinearLayout(context);
                info.setOrientation(LinearLayout.VERTICAL);

                //titre
                TextView title = new TextView(context);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                //Texte
                TextView snippet = new TextView(context);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        };
    }
}
