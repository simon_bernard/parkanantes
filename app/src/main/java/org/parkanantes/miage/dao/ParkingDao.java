package org.parkanantes.miage.dao;

import android.util.Log;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import org.parkanantes.miage.model.database.Parking;
import org.parkanantes.miage.model.disponibilites.GroupeParking;
import org.parkanantes.miage.model.referentiel.Data;
import org.parkanantes.miage.tools.Utils;

import java.util.List;

import static org.parkanantes.miage.tools.Utils.ouiNonToBoolean;

/**
 * Classe dao pour gérer l'intéraction avec les parkings en base de données
 */
public class ParkingDao {

    private static final String TAG = ParkingDao.class.getName();

    private ParkingDao() {
        // private constructor
    }

    public static List<Parking> getAllParkings() {
        return new Select()
                .from(Parking.class)
                .execute();
    }

    public static Parking getParkingById(String id) {
        try {
            return new Select()
                    .from(Parking.class)
                    .where("id_parking=?", id)
                    .executeSingle();
        } catch (Exception e) {
            Log.e(TAG, "Erreur dans getParkingById : ", e);
        }
        return null;
    }

    public static List<Parking> getFavoriteParkings() {
        return new Select()
                .from(Parking.class)
                .where("is_favorite='1'")
                .execute();
    }

    public static boolean clearAllFavorites() {
        boolean clearOk = false;
        try {
            List<Parking> favoriteParkings = new Select()
                .from(Parking.class)
                .where("is_favorite='1'")
                .execute();
            for (Parking parking : favoriteParkings) {
                changeParkingIsFavorite(false, parking.getIdParking());
            }
            clearOk = true;
        } catch (Exception e) {
            Log.e(TAG, "clearAllFavorites : ", e);

        }
        return clearOk;
    }

    public static List<Parking> getParkingsFromName(String name) {
        return new Select()
                .from(Parking.class)
                .where("nom like '%" + name + "%'")
                .execute();
    }

    public static List<Parking> getParkingsFromPaymentMeans(List<String> paymentMeans) {
        From query =  new Select().from(Parking.class);
        for (int i = 0; i < paymentMeans.size(); i++) {
            String queryClause = "moyens_paiement like '%" + paymentMeans.get(i) + "%'";
            if (i == 0) {
                query = query.where(queryClause);
            } else {
                query = query.or(queryClause);
            }
        }
        return query.execute();
    }

    public static List<Parking> getParkingsFromAvailablePlaces(int availablePlaces) {
        return new Select()
                .from(Parking.class)
                .where("places_disponibles >= " + availablePlaces)
                .execute();
    }

    public static void updateParkingWithDispo(final GroupeParking dataFromAPI){
        try {
            new Update(Parking.class)
                .set("places_disponibles = ? , parking_statut = ? ", dataFromAPI.grpDisponible, Utils.getParkingStatutFromStatutId(dataFromAPI))
                .where("id_parking = ?", dataFromAPI.idObj)
                .execute();
        } catch (Exception e) {
            Log.e(TAG, "updateParkingWithDispo : ", e);
        }
    }

    public static void changeParkingIsFavorite(final boolean isFavorite, final String parkingId){
        String isFavoriteDb = isFavorite ? "1" : "0";
        new Update(Parking.class)
                .set("is_favorite = ?", isFavoriteDb)
                .where("id_parking = ?", parkingId)
                .execute();
    }

    public static void insertParking(final Data dataFromAPI){
        Parking.builder()
                .idParking(String.valueOf(dataFromAPI.idObjet))
                .nom(dataFromAPI.geo.name)
                .presentation(dataFromAPI.presentation)
                .categorie(dataFromAPI.libCategorie)
                .type(dataFromAPI.libType)
                .infoComplementaire(dataFromAPI.infosComplementaires)
                .website(dataFromAPI.siteWeb)
                .accessTransportEnCommun(dataFromAPI.accesTransportsCommuns)
                .stationnementVelo(ouiNonToBoolean(dataFromAPI.stationnementVelo))
                .stationnementVeloSecurise(ouiNonToBoolean(dataFromAPI.stationnementVeloSecurise))
                .serviceVelo(dataFromAPI.serviceVelo)
                .accesPmr(ouiNonToBoolean(dataFromAPI.accesPmr))
                .codePostal(dataFromAPI.codePostal)
                .commune(dataFromAPI.commun)
                .adresse(dataFromAPI.adresse)
                .latitude(dataFromAPI.coordonnees[0])
                .longitude(dataFromAPI.coordonnees[1])
                .telephone(dataFromAPI.telephone)
                .moyensPaiement(dataFromAPI.moyenPaiement)
                .capaciteVoiture(dataFromAPI.capaciteVoiture)
                .capaciteVehiculeElectrique(dataFromAPI.capaciteVehiculeElectrique)
                .capaciteMoto(dataFromAPI.capaciteMoto)
                .capaciteVelo(dataFromAPI.capaciteVelo)
                .capacitePmr(dataFromAPI.capacitePmr).build().save();
    }
}
