package org.parkanantes.miage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import org.parkanantes.miage.dao.ParkingDao;
import org.parkanantes.miage.event.EventBusManager;
import org.parkanantes.miage.event.ListParkingsEvent;
import org.parkanantes.miage.model.database.Parking;
import org.parkanantes.miage.services.DisponibilitesSearchService;
import org.parkanantes.miage.tools.Utils;
import org.parkanantes.miage.tools.exception.ExceptionMessageManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activité pour afficher l'écran de recherche de parkings
 */
public class SearchActivity extends AppCompatActivity {

    private static final int AUTOCOMPLETE_MIN_LETTERS_TO_SHOW = 2;

    private static final String ADDRESS_FILTER_CHOICE = "ADDRESS";

    private static final String NAME_FILTER_CHOICE = "NAME";

    private static final String AVAILABLE_PLACES_FILTER_CHOICE = "AVAILABLE_PLACES";

    private static final String PAYMENT_MEAN_FILTER_CHOICE = "PAYMENT_MEAN";

    @BindView(R.id.search_btn_moyen_paiement)
    Button paymentMeanFilterButton;

    @BindView(R.id.search_btn_addresse)
    Button addressFilterButton;

    @BindView(R.id.search_btn_places_disponibles)
    Button availablePlacesFilterButton;

    @BindView(R.id.search_btn_nom)
    Button nameFilterButton;

    @BindView(R.id.adresse_input)
    AutoCompleteTextView addressInput;

    @BindView(R.id.nom_input)
    AutoCompleteTextView nameInput;

    @BindView(R.id.seek_bar)
    SeekBar minAvailablePlacesInput;

    @BindView(R.id.seek_bar_container)
    LinearLayout minAvailablePlacesContainer;

    @BindView(R.id.seek_bar_info)
    TextView seekBarInfo;

    @BindView(R.id.moyens_paiment_container)
    LinearLayout paymentMeanContainer;

    private String filterChoice = PAYMENT_MEAN_FILTER_CHOICE; // par défaut

    private List<Parking> parkings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_layout);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.search_btn_moyen_paiement)
    public void onPaymentMeanFilterButtonClick() {
        resetUi();
        paymentMeanFilterButton.setBackgroundResource(R.drawable.selected_rounded_button);
        paymentMeanContainer.setVisibility(View.VISIBLE);
        filterChoice = PAYMENT_MEAN_FILTER_CHOICE;
    }

    @OnClick(R.id.search_btn_addresse)
    public void onAddressFilterButtonClick() {
        resetUi();
        addressFilterButton.setBackgroundResource(R.drawable.selected_rounded_button);
        addressInput.setVisibility(View.VISIBLE);
        filterChoice = ADDRESS_FILTER_CHOICE;
    }

    @OnClick(R.id.search_btn_places_disponibles)
    public void onAvailablePlacesFilterButtonClick() {
        resetUi();
        availablePlacesFilterButton.setBackgroundResource(R.drawable.selected_rounded_button);
        minAvailablePlacesContainer.setVisibility(View.VISIBLE);
        filterChoice = AVAILABLE_PLACES_FILTER_CHOICE;
    }

    @OnClick(R.id.search_btn_nom)
    public void onNameFilterButtonClick() {
        resetUi();
        nameFilterButton.setBackgroundResource(R.drawable.selected_rounded_button);
        nameInput.setVisibility(View.VISIBLE);
        filterChoice = NAME_FILTER_CHOICE;
    }

    private void resetUi() {
        // les boutons en non selectionnés
        paymentMeanFilterButton.setBackgroundResource(R.drawable.unselected_rounded_button);
        addressFilterButton.setBackgroundResource(R.drawable.unselected_rounded_button);
        availablePlacesFilterButton.setBackgroundResource(R.drawable.unselected_rounded_button);
        nameFilterButton.setBackgroundResource(R.drawable.unselected_rounded_button);

        // les input des filtres en invisibles
        addressInput.setVisibility(View.INVISIBLE);
        nameInput.setVisibility(View.INVISIBLE);
        minAvailablePlacesContainer.setVisibility(View.INVISIBLE);
        paymentMeanContainer.setVisibility(View.INVISIBLE);
    }

    private void setUpAutoCompleteTextViews(List<Parking> parkings) {

        List<String> names = new ArrayList<>();
        List<String> adresses = new ArrayList<>();
        for (Parking parking : parkings) {
            names.add(parking.getNom());
            adresses.add(Utils.getAddressFromParking(parking));
        }
        ArrayAdapter<String> namesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
        ArrayAdapter<String> addressesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, adresses);

        addressInput.setAdapter(addressesAdapter);
        addressInput.setThreshold(AUTOCOMPLETE_MIN_LETTERS_TO_SHOW);

        nameInput.setAdapter(namesAdapter);
        nameInput.setThreshold(AUTOCOMPLETE_MIN_LETTERS_TO_SHOW);
    }

    private void setUpPaymentMeansCheckBox(List<Parking> parkings) {
        List<String> paymentMeans = Utils.getUniquePaymentMeans(parkings);
        paymentMeanContainer.removeAllViews();
        for (String paymentMean : paymentMeans) {
            CheckBox checkBox = new CheckBox(this);
            checkBox.setText(paymentMean);
            paymentMeanContainer.addView(checkBox);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        DisponibilitesSearchService.INSTANCE.searchDisponibilites();
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void errorOccured(final Exception error) {
        ExceptionMessageManager.showException(error, this);
    }

    @Subscribe
    public void allParkingDispoResult(ListParkingsEvent parkings) {
        if (!parkings.getParkings().isEmpty()) {
            // setter le max value de la seekbar + le texte à afficher avec data de la db
            this.parkings = parkings.getParkings();
            final int maxAvailablePlacesToEnter = Utils.getMaxAvailablePlaces(this.parkings);
            minAvailablePlacesInput.setMax(maxAvailablePlacesToEnter);
            minAvailablePlacesInput.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    String text = progress + getResources()
                            .getString(R.string.seek_bar_info_message)
                            .replace("{MAX}", String.valueOf(maxAvailablePlacesToEnter));
                    seekBarInfo.setText(text);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });

            String seekBarInfoMessage = getResources()
                    .getString(R.string.seek_bar_info_message)
                    .replace("{MAX}", String.valueOf(maxAvailablePlacesToEnter));
            seekBarInfo.setText(0 + seekBarInfoMessage);

            setUpAutoCompleteTextViews(this.parkings);
            setUpPaymentMeansCheckBox(this.parkings);
        }
    }

    @OnClick(R.id.search_button)
    public void onSearchButtonClick() {
        switch (filterChoice) {
            case ADDRESS_FILTER_CHOICE :
                onAddressFilterChoice();
                break;

            case NAME_FILTER_CHOICE :
                onNameFilterChoice();
                break;

            case AVAILABLE_PLACES_FILTER_CHOICE :
                onAvailablePlacesFilterChoice();
                break;

            case PAYMENT_MEAN_FILTER_CHOICE :
                onPaymentMeanFilterChoice();
                break;

            default :
                break;
        }
    }

    private void onAddressFilterChoice() {
        if (addressInput.getText().toString().isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.address_filter_message), Toast.LENGTH_LONG).show();
        } else {
            // Pas possible de mettre une adresse complète pour une recherche (séparation plus casse non gérés)
            List<Parking> parkingsToSend = Utils.getParkingsFromAddresses(this.parkings, addressInput.getText().toString());
            sendToListParkingActivity(parkingsToSend);
        }
    }

    private void onNameFilterChoice() {
        if (nameInput.getText().toString().isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.name_filter_message), Toast.LENGTH_LONG).show();
        } else {
            List<Parking> parkingsToSend = ParkingDao.getParkingsFromName(nameInput.getText().toString());
            sendToListParkingActivity(parkingsToSend);
        }
    }

    private void onAvailablePlacesFilterChoice() {
        if (0 == minAvailablePlacesInput.getProgress()) {
            Toast.makeText(this, getResources().getString(R.string.available_place_filter_message), Toast.LENGTH_LONG).show();
        } else {
            List<Parking> parkingsToSend = ParkingDao.getParkingsFromAvailablePlaces(minAvailablePlacesInput.getProgress());
            sendToListParkingActivity(parkingsToSend);
        }
    }

    private void onPaymentMeanFilterChoice() {
        // vérifier au moins une checkbox ok
        List<View> checkboxes = paymentMeanContainer.getTouchables();
        List<String> paymentMeansSelected = new ArrayList<>();
        for (int i = 0; i < checkboxes.size(); i++) {
            if (checkboxes.get(i) instanceof CheckBox) {
                CheckBox checkbox = (CheckBox) checkboxes.get(i);
                if (checkbox.isChecked()) {
                    paymentMeansSelected.add(checkbox.getText().toString());
                }
            }
        }
        if (paymentMeansSelected.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.payment_mean_filter_message), Toast.LENGTH_LONG).show();
        } else {
            List<Parking> parkingsToSend = ParkingDao.getParkingsFromPaymentMeans(paymentMeansSelected);
            sendToListParkingActivity(parkingsToSend);
        }
    }

    private void sendToListParkingActivity(List<Parking> parkings) {
        if (parkings.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.no_parking), Toast.LENGTH_LONG).show();
        } else {
            ArrayList<String> ids = Utils.getIdsFromParkingsList(parkings);
            Intent intent = new Intent(this, ListParkingActivity.class);
            intent.putExtra(Utils.EXTRA_SEARCH_ACTIVITY, true);
            intent.putExtra(Utils.EXTRA_IDS, ids);
            startActivity(intent);
        }
    }

}
