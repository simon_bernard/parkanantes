package org.parkanantes.miage.model.disponibilites;

import com.google.gson.annotations.Expose;

/**
 * Classe correspondant à la réponse générale de l'api getDisponibilites
 * Seules les données de l'attribut "data" sont récupérées
 */
public class Answer {

    @Expose
    public Data data;
}
