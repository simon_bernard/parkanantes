package org.parkanantes.miage.model.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


/**
 * Classe représentant un parking en base de données
 */
@Table(name = "Parking")
@Builder
@Getter
@Setter
public class Parking extends Model {

    @Column(name = "id_parking", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String idParking;

    @Column(name = "is_favorite")
    private boolean isFavorite;

    // Infos sur le parking

    @Column(name = "nom")
    private String nom;

    @Column(name = "presentation")
    private String presentation;

    @Column(name = "categorie")
    private String categorie;

    @Column(name = "type")
    private String type;

    @Column(name = "info_complementaire")
    private String infoComplementaire;

    @Column(name = "website")
    private String website;

    @Column(name = "horodatage")
    private String horodatage;

    @Column(name = "access_transport_en_commun")
    private String accessTransportEnCommun;

    @Column(name = "stationnement_velo")
    private boolean stationnementVelo;

    @Column(name = "stationnement_velo_securise")
    private boolean stationnementVeloSecurise;

    @Column(name = "service_velo")
    private String serviceVelo;

    @Column(name = "acces_pmr")
    private boolean accesPmr;

    // Infos sur la localisation et contact

    @Column(name = "code_postal")
    private int codePostal;

    @Column(name = "commune")
    private String commune;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "moyens_paiement")
    private String moyensPaiement;

    // Infos sur les capacités

    @Column(name = "parking_statut")
    private String parkingStatut;

    @Column(name = "places_disponibles")
    private int placesDisponibles;

    @Column(name = "capacite_voiture")
    private int capaciteVoiture;

    @Column(name = "capacite_vehicule_electrique")
    private int capaciteVehiculeElectrique;

    @Column(name = "capacite_moto")
    private int capaciteMoto;

    @Column(name = "capacite_velo")
    private int capaciteVelo;

    @Column(name = "capacite_pmr")
    private int capacitePmr;

    public Parking() {
        super();
    }

    public Parking(String idParking, boolean isFavorite, String nom, String presentation, String categorie, String type, String infoComplementaire, String website, String horodatage, String accessTransportEnCommun, boolean stationnementVelo , boolean stationnementVeloSecurise, String serviceVelo, boolean accesPmr, int codePostal, String commune, String adresse, String latitude, String longitude, String telephone, String moyensPaiement, String parkingStatut, int placesDisponibles, int capaciteVoiture, int capaciteVehiculeElectrique, int capaciteMoto, int capaciteVelo, int capacitePmr) {
        super();
        this.idParking = idParking;
        this.isFavorite = isFavorite;
        this.nom = nom;
        this.presentation = presentation;
        this.categorie = categorie;
        this.type = type;
        this.infoComplementaire = infoComplementaire;
        this.website = website;
        this.horodatage = horodatage;
        this.accessTransportEnCommun = accessTransportEnCommun;
        this.stationnementVelo = stationnementVelo;
        this.stationnementVeloSecurise = stationnementVeloSecurise;
        this.serviceVelo = serviceVelo;
        this.accesPmr = accesPmr;
        this.codePostal = codePostal;
        this.commune = commune;
        this.adresse = adresse;
        this.latitude = latitude;
        this.longitude = longitude;
        this.telephone = telephone;
        this.moyensPaiement = moyensPaiement;
        this.placesDisponibles = placesDisponibles;
        this.parkingStatut = parkingStatut;
        this.capaciteVoiture = capaciteVoiture;
        this.capaciteVehiculeElectrique = capaciteVehiculeElectrique;
        this.capaciteMoto = capaciteMoto;
        this.capaciteVelo = capaciteVelo;
        this.capacitePmr = capacitePmr;
    }
}
