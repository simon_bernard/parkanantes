package org.parkanantes.miage.model.disponibilites;

import com.google.gson.annotations.Expose;

/**
 * Classe correspondant à la réponse de l'api getDisponibilites
 * La réponse générale est stockée dans l'objet Answer
 */
public class OpenData {

    @Expose
    public Answer answer;
}
