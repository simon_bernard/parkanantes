package org.parkanantes.miage.model.disponibilites;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Classe correspondant à l'attribut "Groupe_Parking" de la réponse de l'api getDisponibilites
 * On y récupère la liste de tous les parkings de l'api.
 */
public class GroupesParking {

    @Expose
    @SerializedName("Groupe_Parking")
    public List<GroupeParking> groupeParking;
}
