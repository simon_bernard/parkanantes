package org.parkanantes.miage.model.disponibilites;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Classe correspondant à l'attribut "data" de la réponse de l'api getDisponibilites
 * On y récupère le groupe des parkings
 */
public class Data {

    @Expose
    @SerializedName("Groupes_Parking")
    public GroupesParking groupesParking;
}
