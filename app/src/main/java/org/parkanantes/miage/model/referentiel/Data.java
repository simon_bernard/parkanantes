package org.parkanantes.miage.model.referentiel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Classe contenant les informations pour un parking dans de la réponse de l'api référentiel
 */
public class Data {

    @Expose
    @SerializedName("SERVICE_VELO")
    public String serviceVelo;

    @Expose
    @SerializedName("CAPACITE_VOITURE")
    public int capaciteVoiture;

    @Expose
    @SerializedName("STATIONNEMENT_VELO")
    public String stationnementVelo;

    @Expose
    @SerializedName("INFOS_COMPLEMENTAIRES")
    public String infosComplementaires;

    @Expose
    @SerializedName("STATIONNEMENT_VELO_SECURISE")
    public String stationnementVeloSecurise;

    @Expose
    @SerializedName("geo")
    public Geo geo;

    @Expose
    @SerializedName("_l")
    public String[] coordonnees;

    @Expose
    @SerializedName("CAPACITE_VEHICULE_ELECTRIQUE")
    public int capaciteVehiculeElectrique;

    @Expose
    @SerializedName("CODE_POSTAL")
    public int codePostal;

    @Expose
    @SerializedName("ACCES_PMR")
    public String accesPmr;

    @Expose
    @SerializedName("TELEPHONE")
    public String telephone;

    @Expose
    @SerializedName("MOYEN_PAIEMENT")
    public String moyenPaiement;

    @Expose
    @SerializedName("ACCES_TRANSPORTS_COMMUNS")
    public String accesTransportsCommuns;

    @Expose
    @SerializedName("CAPACITE_MOTO")
    public int capaciteMoto;

    @Expose
    @SerializedName("CONDITIONS_D_ACCES")
    public String conditionsAcces;

    @Expose
    @SerializedName("COMMUNE")
    public String commun;

    @Expose
    @SerializedName("CAPACITE_VELO")
    public int capaciteVelo;

    @Expose
    @SerializedName("PRESENTATION")
    public String presentation;

    @Expose
    @SerializedName("CAPACITE_PMR")
    public int capacitePmr;

    @Expose
    @SerializedName("LIBCATEGORIE")
    public String libCategorie;

    @Expose
    @SerializedName("EXPLOITANT")
    public String exploitant;

    @Expose
    @SerializedName("SITE_WEB")
    public String siteWeb;

    @Expose
    @SerializedName("ADRESSE")
    public String adresse;

    @Expose
    @SerializedName("LIBTYPE")
    public String libType;

    @Expose
    @SerializedName("_IDOBJ")
    public int idObjet;

}
