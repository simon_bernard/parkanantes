package org.parkanantes.miage.model.disponibilites;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Classe correspondant à l'objet gérant la réponse de l'api getDisponibilites à travers le pattern
 * Event Bus. Elle contient l'objet OpenData qui contient cette réponse.
 */
public class DisponibilitesSearchResult {

    @Expose
    @SerializedName("opendata")
    public OpenData opendata;

}
