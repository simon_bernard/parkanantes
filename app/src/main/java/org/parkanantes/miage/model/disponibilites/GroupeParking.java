package org.parkanantes.miage.model.disponibilites;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
* Classe contenant les informations pour un parking dans de la réponse de l'api getDisponibilites
*/
public class GroupeParking {

    @Expose
    @SerializedName("Grp_identifiant")
    public String grpIdentifiant;

    @Expose
    @SerializedName("Grp_nom")
    public String grpNom;

    @Expose
    @SerializedName("Grp_statut")
    public String grpStatut;

    @Expose
    @SerializedName("Grp_pri_aut")
    public String grpPriAut;

    @Expose
    @SerializedName("Grp_disponible")
    public String grpDisponible;

    @Expose
    @SerializedName("Grp_complet")
    public String grpComplet;

    @Expose
    @SerializedName("Grp_exploitation")
    public String grpExploitation;

    @Expose
    @SerializedName("Grp_horodatage")
    public String grpHorodatage;

    @Expose
    @SerializedName("IdObj")
    public String idObj;
}
