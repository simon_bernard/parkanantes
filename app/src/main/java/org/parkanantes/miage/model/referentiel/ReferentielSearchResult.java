package org.parkanantes.miage.model.referentiel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Classe correspondant à l'objet gérant la réponse de l'api rérérentiel à travers le pattern
 * Event Bus.
 */
public class ReferentielSearchResult {

    @Expose
    @SerializedName("version")
    public String version;

    @Expose
    @SerializedName("nb_results")
    public int nbResults;

    @Expose
    @SerializedName("data")
    public List<Data> data;
}
