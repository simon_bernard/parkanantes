package org.parkanantes.miage.model.referentiel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Classe contenant les informations de l'attribut "geo" dans de la réponse de l'api référentiel.
 * Il s'agit du nom du parking.
 */
public class Geo {

    @Expose
    @SerializedName("name")
    public String name;
}
