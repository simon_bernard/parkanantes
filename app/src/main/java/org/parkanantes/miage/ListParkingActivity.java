package org.parkanantes.miage;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import org.parkanantes.miage.dao.ParkingDao;
import org.parkanantes.miage.event.EventBusManager;
import org.parkanantes.miage.event.ListParkingsEvent;
import org.parkanantes.miage.model.database.Parking;
import org.parkanantes.miage.services.DisponibilitesSearchService;
import org.parkanantes.miage.tools.CardAdapter;
import org.parkanantes.miage.tools.Utils;
import org.parkanantes.miage.tools.exception.ExceptionMessageManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activité pour afficher la liste des parkings.
 * Des paramètres peuvent y être passés :
 * EXTRA_SEARCH_ACTIVITY & EXTRA_IDS : affichage depuis une recherche, contient la liste des ids des parkings à afficher
 * EXTRA_FAVORITE_PARKINGS : afficher les parkings favoris
 * S'il n'y a pas de paramètres, l'ensemble des parkings seront affichés
 */
public class ListParkingActivity extends AppCompatActivity {

    @BindView(R.id.my_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.warning_message)
    TextView warningMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_parking_layout);
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        manageCallFromOtherActivities();
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void allParkingDispoResult(ListParkingsEvent parkings) {
        putParkingsIntoList(parkings.getParkings());
    }

    @Subscribe
    public void errorOccured(final Exception error) {
        ExceptionMessageManager.showException(error, this);
    }

    private void manageClearFavoritesButton() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(actionBar.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);
        ImageView imageView = new ImageView(actionBar.getThemedContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setImageResource(R.drawable.trash);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 40;
        imageView.setLayoutParams(layoutParams);
        actionBar.setCustomView(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageClearButtonOnClick();
            }
        });
    }

    private void manageClearButtonOnClick() {
        final Context context = this;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getResources().getString(R.string.favorites_clear_alert_title))
                .setMessage(getResources().getString(R.string.favorites_clear_alert_message))
                .setPositiveButton(getResources().getString(R.string.oui), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (ParkingDao.clearAllFavorites()) {
                            DisponibilitesSearchService.INSTANCE.searchDisponibilitesFav();
                            Toast.makeText(context, context.getResources()
                                            .getString(R.string.favorites_clear_alert_ok),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, context.getResources()
                                            .getString(R.string.favorites_clear_alert_ko),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton(getResources().getString(R.string.non), null)
                .show();
    }

    private void manageCallFromOtherActivities() {
        if (getIntent() != null
                && getIntent().getExtras() != null) {
            // On vient d'une autre activité qui a déjà refresh les données, on doit juste les mettre dans la liste
            if (getIntent().getExtras().getBoolean(Utils.EXTRA_SEARCH_ACTIVITY, false)) {
                ArrayList<String> ids = (ArrayList<String>) getIntent().getSerializableExtra(Utils.EXTRA_IDS);
                List<Parking> parkings = new ArrayList<>();
                for (String id : ids) {
                    parkings.add(ParkingDao.getParkingById(id));
                }
                putParkingsIntoList(parkings);
            } else if (getIntent().getExtras().getBoolean(Utils.EXTRA_FAVORITE_PARKINGS, false)) {
                DisponibilitesSearchService.INSTANCE.searchDisponibilitesFav();
                manageClearFavoritesButton();
            }
        } else {
            // Appel depuis la main activity, on doit refresh les données
            DisponibilitesSearchService.INSTANCE.searchDisponibilites();
        }
    }

    private void putParkingsIntoList(List<Parking> parkingList) {
        if (parkingList.isEmpty()) {
            warningMessage.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            warningMessage.setVisibility(View.INVISIBLE);
            CardAdapter cardAdapter = new CardAdapter(parkingList, this);
            recyclerView.setAdapter(cardAdapter);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

}
