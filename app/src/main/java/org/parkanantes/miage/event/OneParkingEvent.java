package org.parkanantes.miage.event;

import org.parkanantes.miage.model.database.Parking;

import lombok.Getter;

/**
 * Classe représentant un évènement contenant un seul parking
 * (Utilisé lors de la récupération ou de la mise à jour des informations pour un seul parking)
 */
@Getter
public class OneParkingEvent {

    private Parking parking;

    public OneParkingEvent(Parking parking){
        this.parking = parking;
    }

}
