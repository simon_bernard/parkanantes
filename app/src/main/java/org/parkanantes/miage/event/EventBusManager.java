package org.parkanantes.miage.event;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Classe poru gérer un évènement à travers le bus d'évènements (Pattern Event Bus)
 */
public class EventBusManager {

    private EventBusManager() {
        // private constructor
    }

    public static final Bus BUS = new Bus(ThreadEnforcer.ANY);
}
