package org.parkanantes.miage.event;

import org.parkanantes.miage.model.database.Parking;

import java.util.List;

import lombok.Getter;

/**
 * Classe représentant un évènement contenant plusieurs parkings
 */
@Getter
public class ListParkingsEvent {

    private List<Parking> parkings;

    public ListParkingsEvent(List<Parking> parkings) {
        this.parkings = parkings;
    }
}
