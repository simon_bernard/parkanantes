package org.parkanantes.miage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import org.parkanantes.miage.event.EventBusManager;
import org.parkanantes.miage.model.referentiel.ReferentielSearchResult;
import org.parkanantes.miage.services.ReferentielSearchService;
import org.parkanantes.miage.tools.Utils;
import org.parkanantes.miage.tools.exception.ExceptionMessageManager;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activité principale pour afficher le menu
 */
public class MainActivity extends AppCompatActivity {

    private static final String SHARED_PREFERENCES_NAME = "PARKANANTES_SHARED_PREFERENCES";

    private static final String LAST_UPDATE_PREFERENCE_KEY = "lastUpdate";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Vérification si l'update du référentiel a été fait il y a plus de 7 jours
        SharedPreferences prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        String lastUpdate = prefs.getString(LAST_UPDATE_PREFERENCE_KEY, null);
        if (lastUpdate == null && !Utils.isOnline(this)) {
            Toast.makeText(this, R.string.no_data_first_connexion_exception_message,
                    Toast.LENGTH_LONG).show();
        } else if (lastUpdate == null || Utils.isOlderThanSevenDays(lastUpdate)) {
            // appel référentiel
            Toast.makeText(this, getResources().getString(R.string.referentiel_update_warning),
                    Toast.LENGTH_LONG).show();
            ReferentielSearchService.INSTANCE.searchReferentiel();
        }
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        EventBusManager.BUS.register(this);
        super.onResume();
    }

    @Subscribe
    public void referentielApiCallResponse(final ReferentielSearchResult event) {
        // Mise à jour des sharedPreferences
        SharedPreferences.Editor editor = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE).edit();
        editor.putString(LAST_UPDATE_PREFERENCE_KEY, Utils.getTodayDateAsString());
        editor.apply();
        Toast.makeText(this, getResources().getString(R.string.referentiel_update_ok),
                Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void errorOccured(final Exception error) {
        ExceptionMessageManager.showException(error, this);
    }

    @OnClick({R.id.rechercher_un_parking_area, R.id.rechercher_un_parking_image})
    public void clickedOnRechercherUnParking() {
        Intent switchToSearchIntent = new Intent(this, SearchActivity.class);
        startActivity(switchToSearchIntent);
    }

    @OnClick({R.id.carte_des_parking_area, R.id.carte_des_parking_image})
    public void clickedOnCarteDesParking() {
        Intent switchToMapIntent = new Intent(this, MapActivity.class);
        startActivity(switchToMapIntent);
    }

    @OnClick({R.id.liste_des_parkings_area, R.id.liste_des_parkings_image})
    public void clickedOnListeDesParking() {
        Intent switchToListIntent = new Intent(this, ListParkingActivity.class);
        startActivity(switchToListIntent);
    }

    @OnClick({R.id.parking_favoris_area, R.id.parking_favoris_image})
    public void clickedOnParkingFavoris() {
        Intent switchToListIntent = new Intent(this, ListParkingActivity.class);
        switchToListIntent.putExtra(Utils.EXTRA_FAVORITE_PARKINGS, true);
        startActivity(switchToListIntent);
    }
}
