package org.parkanantes.miage.services;

import org.parkanantes.miage.model.referentiel.ReferentielSearchResult;
import org.parkanantes.miage.tools.ApiConstants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface exposant les méthodes pour appeler l'api référentiel
 */
public interface ReferentielSearchRESTService {

    /**
     * Appel à l'api référentiel de Nantes Métropole
     * @param format le format de fichier attendu
     * @return Un évènement contenant la réponse de l'api
     */
    @GET(ApiConstants.API_REFERENTIEL_URL)
    Call<ReferentielSearchResult> searchForReferentiel(@Query("format") String format);
}
