package org.parkanantes.miage.services;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.parkanantes.miage.R;
import org.parkanantes.miage.dao.ParkingDao;
import org.parkanantes.miage.event.EventBusManager;
import org.parkanantes.miage.event.ListParkingsEvent;
import org.parkanantes.miage.event.OneParkingEvent;
import org.parkanantes.miage.model.disponibilites.DisponibilitesSearchResult;
import org.parkanantes.miage.model.disponibilites.GroupeParking;
import org.parkanantes.miage.tools.ApiConstants;
import org.parkanantes.miage.tools.exception.CustomComplexException;
import org.parkanantes.miage.tools.exception.CustomSimpleException;
import org.parkanantes.miage.tools.exception.ExceptionMessageManager;

import java.lang.reflect.Modifier;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisponibilitesSearchService {

    public static final DisponibilitesSearchService INSTANCE = new DisponibilitesSearchService();

    private Gson gsonConverter;
    private DisponibilitesSearchRESTService restService;

    private DisponibilitesSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        gsonConverter = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
    }

    public void searchDisponibilites(){
        EventBusManager.BUS.post(new ListParkingsEvent(ParkingDao.getAllParkings()));
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(new OkHttpClient())
                    .baseUrl(ApiConstants.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                    .build();
            restService = retrofit.create(DisponibilitesSearchRESTService.class);

            restService.searchForDisponibilities(ApiConstants.JSON_FORMAT).enqueue(new Callback<DisponibilitesSearchResult>() {
                @Override
                public void onResponse(Call<DisponibilitesSearchResult> call, Response<DisponibilitesSearchResult> response) {
                    if (response != null && response.body() != null) {
                         for (GroupeParking dataFromAPI : response.body().opendata.answer.data.groupesParking.groupeParking ){
                             ParkingDao.updateParkingWithDispo(dataFromAPI);
                        }
                        EventBusManager.BUS.post(response.body());
                        EventBusManager.BUS.post(new ListParkingsEvent(ParkingDao.getAllParkings()));
                    } else {
                        EventBusManager.BUS.post(new CustomSimpleException(R.string.null_response_message));
                    }
                }

                @Override
                public void onFailure(Call<DisponibilitesSearchResult> call, Throwable t) {
                    CustomComplexException customException = ExceptionMessageManager.manageExceptionMessage(t);
                    customException.setMessageHeadingId(R.string.api_call_failure_message);
                    EventBusManager.BUS.post(customException);
                }
            });
        } catch (Exception e) {
            CustomComplexException customComplexException = new CustomComplexException(0, false, e);
            customComplexException.setMessageHeadingId(R.string.exception_message);
            EventBusManager.BUS.post(customComplexException);
        }
    }


    public void searchDisponibilitesById(final String idParking) {
        EventBusManager.BUS.post(new OneParkingEvent(ParkingDao.getParkingById(idParking)));

        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(new OkHttpClient())
                    .baseUrl(ApiConstants.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                    .build();
            restService = retrofit.create(DisponibilitesSearchRESTService.class);

            restService.searchForDisponibilities(ApiConstants.JSON_FORMAT).enqueue(new Callback<DisponibilitesSearchResult>() {
                @Override
                public void onResponse(Call<DisponibilitesSearchResult> call, Response<DisponibilitesSearchResult> response) {
                    if (response != null && response.body() != null) {
                        for (GroupeParking dataFromAPI : response.body().opendata.answer.data.groupesParking.groupeParking ){
                            ParkingDao.updateParkingWithDispo(dataFromAPI);
                        }
                        EventBusManager.BUS.post(new OneParkingEvent(ParkingDao.getParkingById(idParking)));
                    } else {
                        EventBusManager.BUS.post(new CustomSimpleException(R.string.null_response_message));
                    }
                }

                @Override
                public void onFailure(Call<DisponibilitesSearchResult> call, Throwable t) {
                    CustomComplexException customException = ExceptionMessageManager.manageExceptionMessage(t);
                    customException.setMessageHeadingId(R.string.api_call_failure_message);
                    EventBusManager.BUS.post(customException);
                }
            });
        } catch (Exception e) {
            CustomComplexException customComplexException = new CustomComplexException(0, false, e);
            customComplexException.setMessageHeadingId(R.string.exception_message);
            EventBusManager.BUS.post(customComplexException);
        }
    }

    public void searchDisponibilitesFav(){
        EventBusManager.BUS.post(new ListParkingsEvent(ParkingDao.getFavoriteParkings()));
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(new OkHttpClient())
                    .baseUrl(ApiConstants.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                    .build();
            restService = retrofit.create(DisponibilitesSearchRESTService.class);

            restService.searchForDisponibilities(ApiConstants.JSON_FORMAT).enqueue(new Callback<DisponibilitesSearchResult>() {
                @Override
                public void onResponse(Call<DisponibilitesSearchResult> call, Response<DisponibilitesSearchResult> response) {
                    if (response != null && response.body() != null) {
                        for (GroupeParking dataFromAPI : response.body().opendata.answer.data.groupesParking.groupeParking ){
                            ParkingDao.updateParkingWithDispo(dataFromAPI);
                        }
                        EventBusManager.BUS.post(new ListParkingsEvent(ParkingDao.getFavoriteParkings()));
                    } else {
                        EventBusManager.BUS.post(new CustomSimpleException(R.string.null_response_message));
                    }
                }

                @Override
                public void onFailure(Call<DisponibilitesSearchResult> call, Throwable t) {
                    CustomComplexException customException = ExceptionMessageManager.manageExceptionMessage(t);
                    customException.setMessageHeadingId(R.string.api_call_failure_message);
                    EventBusManager.BUS.post(customException);
                }
            });
        } catch (Exception e) {
            CustomComplexException customComplexException = new CustomComplexException(0, false, e);
            customComplexException.setMessageHeadingId(R.string.exception_message);
            EventBusManager.BUS.post(customComplexException);
        }
    }
}