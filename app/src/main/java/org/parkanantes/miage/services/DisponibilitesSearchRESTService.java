package org.parkanantes.miage.services;

import org.parkanantes.miage.model.disponibilites.DisponibilitesSearchResult;
import org.parkanantes.miage.tools.ApiConstants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface exposant les méthodes pour appeler l'api getDisponibilites
 */
public interface DisponibilitesSearchRESTService {

    /**
     * Appel à l'api getDisponibilites de Nantes Métropole
     * @param format le format de fichier attendu
     * @return Un évènement contenant la réponse de l'api
     */
    @GET(ApiConstants.API_DISPONIBILITES_URL)
    Call<DisponibilitesSearchResult> searchForDisponibilities(@Query("output") String format);
}
