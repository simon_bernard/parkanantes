package org.parkanantes.miage.services;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.parkanantes.miage.R;
import org.parkanantes.miage.dao.ParkingDao;
import org.parkanantes.miage.event.EventBusManager;
import org.parkanantes.miage.event.ListParkingsEvent;
import org.parkanantes.miage.model.referentiel.ReferentielSearchResult;
import org.parkanantes.miage.tools.ApiConstants;
import org.parkanantes.miage.tools.exception.CustomComplexException;
import org.parkanantes.miage.tools.exception.CustomSimpleException;
import org.parkanantes.miage.tools.exception.ExceptionMessageManager;

import java.lang.reflect.Modifier;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReferentielSearchService {

    public static final ReferentielSearchService INSTANCE = new ReferentielSearchService();

    private Gson gsonConverter;

    private ReferentielSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        gsonConverter = new GsonBuilder()
            .setLenient()
            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
            .serializeNulls()
            .excludeFieldsWithoutExposeAnnotation().create();
    }

    public void searchReferentiel(){
        EventBusManager.BUS.post(new ListParkingsEvent(ParkingDao.getAllParkings()));
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(new OkHttpClient())
                    .baseUrl(ApiConstants.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                    .build();
            ReferentielSearchRESTService restService = retrofit.create(ReferentielSearchRESTService.class);

            restService.searchForReferentiel(ApiConstants.JSON_FORMAT).enqueue(new Callback<ReferentielSearchResult>() {
                @Override
                public void onResponse(Call<ReferentielSearchResult> call, Response<ReferentielSearchResult> response) {
                    if (response != null && response.body() != null) {
                        for (int i = 0; i < response.body().data.size() - 1; i++) {
                             ParkingDao.insertParking(response.body().data.get(i));
                        }
                        EventBusManager.BUS.post(response.body());
                        EventBusManager.BUS.post(new ListParkingsEvent(ParkingDao.getAllParkings()));
                    } else {
                        EventBusManager.BUS.post(new CustomSimpleException(R.string.null_response_message));
                    }
                }

                @Override
                public void onFailure(Call<ReferentielSearchResult> call, Throwable t) {
                    CustomComplexException customException = ExceptionMessageManager.manageExceptionMessage(t);
                    customException.setMessageHeadingId(R.string.api_call_failure_message);
                    EventBusManager.BUS.post(customException);
                }
            });
        } catch (Exception e) {
            CustomComplexException customComplexException = new CustomComplexException(0, false, e);
            customComplexException.setMessageHeadingId(R.string.exception_message);
            EventBusManager.BUS.post(customComplexException);
        }
    }
}
