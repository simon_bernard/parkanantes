package org.parkanantes.miage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.parkanantes.miage.dao.ParkingDao;
import org.parkanantes.miage.event.EventBusManager;
import org.parkanantes.miage.event.OneParkingEvent;
import org.parkanantes.miage.model.database.Parking;
import org.parkanantes.miage.services.DisponibilitesSearchService;
import org.parkanantes.miage.tools.ApiConstants;
import org.parkanantes.miage.tools.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activité pour afficher le détail d'un parking à partir de son id
 */
public class DetailActivity extends AppCompatActivity{

    private static final String URL = "https://maps.googleapis.com/maps/api/streetview?size=600x600&location={0},{1}&heading=151.78&pitch=-0.76&key=AIzaSyCJuwGq3KX1n2tILdod_abZnHgR8RZt1Vo";

    private static final String GOOGLE_MAP_URL = "google.navigation:q={0},{1}";

    private Parking parking;

    @BindView(R.id.detailImageParking)
    ImageView detailImageParking;

    @BindView(R.id.datailNameParking)
    TextView datailNameParking;

    @BindView(R.id.datailStatus)
    TextView datailStatus;

    @BindView(R.id.detailAccesPMR)
    TextView detailAccesPMR;

    @BindView(R.id.datailAddresse)
    TextView datailAddresse;

    @BindView(R.id.detailPostalCode)
    TextView detailPostalCode;

    @BindView(R.id.detailNameTown)
    TextView detailNameTown;

    @BindView(R.id.detailNumberFreeParkingSpace)
    TextView detailNumberFreeParkingSpace;

    @BindView(R.id.detailPhoneNumber)
    TextView detailPhoneNumber;

    @BindView(R.id.detailStationnementVelo)
    TextView detailStationnementVelo;

    @BindView(R.id.detailStationnementVeloSecu)
    TextView detailStationnementVeloSecu;

    @BindView(R.id.detailCapacityAuto)
    TextView detailCapacityAuto;

    @BindView(R.id.detailCapacityAutoElec)
    TextView detailCapacityAutoElec;

    @BindView(R.id.detailCapacityAutoPMR)
    TextView detailCapacityAutoPMR;

    @BindView(R.id.detailCapacityMoto)
    TextView detailCapacityMoto;

    @BindView(R.id.detailCapacityVelo)
    TextView detailCapacityVelo;

    @BindView(R.id.detail_lib_categorie)
    TextView detailLibCategorie;

    @BindView(R.id.detail_libtype)
    TextView detaillibtype;

    @BindView(R.id.labelDetailCategorie)
    TextView labelDetailCategorie;

    @BindView(R.id.labelDetailType)
    TextView labelDetailType;

    @BindView(R.id.label_Detail_Presentation)
    TextView labelDetailPresentation;

    @BindView(R.id.detail_Presentation)
    TextView detailPresentation;

    @BindView(R.id.view3)
    View ligneView3;

    @BindView(R.id.label_Detail_Acces_transport_commun)
    TextView labelDetailAccesTransportCommun;

    @BindView(R.id.detail_Acces_transport_commun)
    TextView detailAccesTransportCommun;

    @BindView(R.id.view4)
    View ligneView4;

    @BindView(R.id.label_detail_service_velo)
    TextView labelDetailServiceVelo;

    @BindView(R.id.detail_service_velo)
    TextView detailServiceVelo;

    @BindView(R.id.view5)
    View ligneView5;

    @BindView(R.id.label_detail_informations_complementaire)
    TextView labelDetailInformationsComplementaire;

    @BindView(R.id.detail_informations_complementaire)
    TextView detailInformationsComplementaire;

    @BindView(R.id.view6)
    View ligneView6;

    @BindView(R.id.detailSiteWeb)
    TextView detailSiteWeb;

    @BindView(R.id.detail_btn_YAller)
    Button detailBtnYAller;

    @BindView(R.id.star_button)
    ImageButton starButton;

    @BindView(R.id.image_cb)
    ImageView imagecb;

    @BindView(R.id.image_cheque)
    ImageView imagecheque;

    @BindView(R.id.image_espece)
    ImageView imageespece;

    @BindView(R.id.image_total)
    ImageView imagetotal;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        DisponibilitesSearchService.INSTANCE.searchDisponibilitesById(getIntent().getStringExtra("id_parking"));
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void allParkingDispoResult(OneParkingEvent parking) {
        this.parking = parking.getParking();
        fillInformation();
    }

    @OnClick(R.id.detail_btn_YAller)
    public void onYAllerButtonClick() {
        Uri gmmIntentUri = Uri.parse(GOOGLE_MAP_URL.replace("{0}", parking.getLatitude()).replace("{1}", parking.getLongitude()));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @OnClick(R.id.star_button)
    public void toggleFavoris() {
        parking.setFavorite(!parking.isFavorite());
        ParkingDao.changeParkingIsFavorite(parking.isFavorite(), parking.getIdParking());
        manageFavoriteStar();
        String info = getResources().getString(parking.isFavorite() ?
                R.string.info_parking_favoris : R.string.info_parking_not_favoris);
        Toast.makeText(this, info, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.detailPhoneNumber)
    public void startIntentCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + parking.getTelephone()));
        startActivity(intent);
    }

    @OnClick(R.id.detailSiteWeb)
    public void startIntentWebSite() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(parking.getWebsite()));
        startActivity(i);
    }



    private void fillInformation() {
        final String urlComplet =  URL.replace("{0}", parking.getLatitude()).replace("{1}", parking.getLongitude());
        Picasso.with(this).load(urlComplet).into(detailImageParking);
        datailNameParking.setText(parking.getNom());
        datailStatus.setText(parking.getParkingStatut());
        datailAddresse.setText(parking.getAdresse());
        detailPostalCode.setText(String.valueOf(parking.getCodePostal()));
        detailNameTown.setText(parking.getCommune());
        detailNumberFreeParkingSpace.setText(String.valueOf(parking.getPlacesDisponibles()));
        detailPhoneNumber.setText(parking.getTelephone());
        detailCapacityAuto.setText(String.valueOf(parking.getCapaciteVoiture()));
        detailCapacityAutoElec.setText(String.valueOf(parking.getCapaciteVehiculeElectrique()));
        detailCapacityAutoPMR.setText(String.valueOf(parking.getCapacitePmr()));
        detailCapacityMoto.setText(String.valueOf(parking.getCapaciteMoto()));
        detailCapacityVelo.setText(String.valueOf(parking.getCapaciteVelo()));
        detailAccesPMR.setText(booleanToString(parking.isAccesPmr()));
        detailStationnementVelo.setText(booleanToString(parking.isStationnementVelo()));
        detailStationnementVeloSecu.setText(booleanToString(parking.isStationnementVeloSecurise()));

        fillAndDisplay(parking.getCategorie(), detailLibCategorie, labelDetailCategorie);
        fillAndDisplay(parking.getType(), detaillibtype, labelDetailType);
        fillAndDisplay(parking.getWebsite(), detailSiteWeb);

        fillAndDisplay(parking.getPresentation(),detailPresentation, labelDetailPresentation, ligneView3);
        fillAndDisplay(parking.getAccessTransportEnCommun(),detailAccesTransportCommun, labelDetailAccesTransportCommun, ligneView4);
        fillAndDisplay(parking.getServiceVelo(),detailServiceVelo, labelDetailServiceVelo, ligneView5);
        fillAndDisplay(parking.getInfoComplementaire(), detailInformationsComplementaire, labelDetailInformationsComplementaire, ligneView6);


        manageFavoriteStar();
        manageMoyenPaiement(parking);
    }

    private void fillAndDisplay(String contenu, TextView textViewPrincipal) {
        if (contenu != null) {
            textViewPrincipal.setVisibility(View.VISIBLE);
            textViewPrincipal.setText(contenu);
        }
    }

    private void fillAndDisplay(String contenu, TextView textViewPrincipal, TextView textViewLabel) {
        if (contenu != null) {
            textViewPrincipal.setVisibility(View.VISIBLE);
            textViewPrincipal.setText(contenu);
            textViewLabel.setVisibility(View.VISIBLE);
        }
    }

    private void fillAndDisplay(String contenu, TextView textViewPrincipal, TextView labelTextsView, View ligne) {
        if (contenu != null) {
            textViewPrincipal.setVisibility(View.VISIBLE);
            textViewPrincipal.setText(contenu);
            labelTextsView.setVisibility(View.VISIBLE);
            ligne.setVisibility(View.VISIBLE);
        }
    }

    private String booleanToString(Boolean bool){
        return bool ? ApiConstants.OUI : ApiConstants.NON;
    }

    private void manageFavoriteStar() {
        if (parking.isFavorite()) {
            starButton.setImageResource(R.drawable.btn_star_big_on);
        } else {
            starButton.setImageResource(R.drawable.btn_star_big_off);
        }
    }

    private void manageMoyenPaiement(Parking parking) {
        List<String> paymentMeans = Utils.getUniquePaymentMeansOneParking(parking);
        if(paymentMeans.contains(ApiConstants.CB_BORNE_SORTIE)){
            imagecb.setVisibility(View.VISIBLE);
        }
        if(paymentMeans.contains(ApiConstants.ESPECE)){
            imageespece.setVisibility(View.VISIBLE);
        }
        if(paymentMeans.contains(ApiConstants.TOTAL_GR)){
            imagetotal.setVisibility(View.VISIBLE);
        }
        if(paymentMeans.contains(ApiConstants.CHEQUE)){
            imagecheque.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.image_cb)
    public void onCbImageClick() {
        Toast.makeText(this, ApiConstants.CB_BORNE_SORTIE, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.image_cheque)
    public void onChequeImageClick() {
        Toast.makeText(this, ApiConstants.CHEQUE, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.image_espece)
    public void onEspeceImageClick() {
        Toast.makeText(this, ApiConstants.ESPECE, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.image_total)
    public void onTotalImageClick() {
        Toast.makeText(this, ApiConstants.TOTAL_GR, Toast.LENGTH_SHORT).show();
    }
}
