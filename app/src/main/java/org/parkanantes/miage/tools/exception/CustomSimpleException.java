package org.parkanantes.miage.tools.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Classe customizé pour gérer une exception afin d'affcher un message contenant :
 * Un contenu, qui est l'id vers le message dans strings.xml
 */
@Getter
@Setter
public class CustomSimpleException extends Exception {

    private int messageId;

    public CustomSimpleException(int messageId) {
        super();
        this.messageId = messageId;
    }
}
