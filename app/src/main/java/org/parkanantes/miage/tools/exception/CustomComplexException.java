package org.parkanantes.miage.tools.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Classe customizé pour gérer une exception afin d'affcher un message contenant :
 * Une entête si "isCustom" est vrai, qui est l'id vers le message dans strings.xml
 * Un contenu, qui est l'id vers le message dans strings.xml
 */
@Getter
@Setter
public class CustomComplexException extends Exception {

    private int messageHeadingId;

    private int messageContentId;

    private boolean isCustom;

    public CustomComplexException(int messageId, boolean isCustom, Throwable exception) {
        super(exception);
        this.messageContentId = messageId;
        this.isCustom = isCustom;
    }
}
