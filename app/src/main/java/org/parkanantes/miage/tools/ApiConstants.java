package org.parkanantes.miage.tools;

/**
 * Classe qui contient les constantes liées aux appels d'api
 */
public class ApiConstants {

    private ApiConstants() {
        // private constructor
    }

    private static final String API_KEY = "74HR0G9GPQ8XA9R";

    public static final String API_BASE_URL = "https://data.nantes.fr";

    public static final String API_DISPONIBILITES_URL = "/api/getDisponibiliteParkingsPublics/1.0/" + API_KEY + "/";

    public static final String API_REFERENTIEL_URL = "/api/publication/24440040400129_NM_NM_00044/LISTE_SERVICES_PKGS_PUB_NM_STBL/content/";

    public static final String JSON_FORMAT = "json";

    public static final String OUVERT = "Ouvert";

    public static final String FERME = "Fermé";

    public static final String ABONNEES = "Abonnés";

    public static final String BUG_API = "BUG API";

    public static final String NOT_DEFINE = "Non défini";

    public static final String COMPLET = "Complet";

    public static final String CB_BORNE_SORTIE = "CB en borne de sortie";

    public static final String ESPECE = "Espèces";

    public static final String TOTAL_GR = "Total GR";

    public static final String CHEQUE = "chèque";

    public static final String OUI = "OUI";

    public static final String NON = "NON";
}
