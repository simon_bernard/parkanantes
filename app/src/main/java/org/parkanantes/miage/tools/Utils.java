package org.parkanantes.miage.tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.parkanantes.miage.model.database.Parking;
import org.parkanantes.miage.model.disponibilites.GroupeParking;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Classe utilitaire pour le projet
 */
public class Utils {

    public static final String EXTRA_FAVORITE_PARKINGS = "forFavoriteParkings";

    public static final String EXTRA_SEARCH_ACTIVITY = "fromSearchActivity";

    public static final String EXTRA_IDS = "ids";

    private static final String SPACE = " ";

    private static final String PAYMENT_MEAN_SEPARATOR = ", ";

    private static final String OUI = "oui";

    private static final String DATE_FORMAT = "dd-M-yyyy";

    private static final String DATE_SEPARATOR = "-";

    private static final int MAX_DAYS_FOR_REFERENTIEL_UPDATE = 7;

    private Utils() {
        // private constructor
    }

    public static String getAddressFromParking(Parking parking) {
        return new StringBuilder()
                .append(parking.getAdresse())
                .append(SPACE)
                .append(parking.getCodePostal())
                .append(SPACE)
                .append(parking.getCommune())
                .toString();
    }

    public static List<String> getUniquePaymentMeans(List<Parking> parkings) {
        Set<String> allPaymentMeans = new HashSet<>();
        for (Parking parking : parkings) {
            if (parking.getMoyensPaiement() != null) {
                String[] paymentMeans = parking.getMoyensPaiement().split(PAYMENT_MEAN_SEPARATOR);
                if (paymentMeans.length > 0) {
                    allPaymentMeans.addAll(Arrays.asList(paymentMeans));
                }
            }
        }
        return new ArrayList<>(allPaymentMeans);
    }

    public static List<String> getUniquePaymentMeansOneParking(Parking parking) {
        Set<String> allPaymentMeans = new HashSet<>();
         if (parking.getMoyensPaiement() != null) {
            String[] paymentMeans = parking.getMoyensPaiement().split(PAYMENT_MEAN_SEPARATOR);
            if (paymentMeans.length > 0) {
                allPaymentMeans.addAll(Arrays.asList(paymentMeans));
            }
         }
        return new ArrayList<>(allPaymentMeans);
    }


    public static boolean ouiNonToBoolean(String ouiNon) {
        return ouiNon.equalsIgnoreCase(OUI);
    }

    public static ArrayList<String> getIdsFromParkingsList(List<Parking> parkings) {
        // ArrayList renvoyée intentionnellement pour être serializable
        ArrayList<String> ids = new ArrayList<>();
        for (Parking parking : parkings) {
            ids.add(parking.getIdParking());
        }
        return ids;
    }

    public static int getMaxAvailablePlaces(List<Parking> parkings) {
        List<Integer> availablesPlaces = new ArrayList<>();
        for (Parking parking : parkings) {
            availablesPlaces.add(parking.getPlacesDisponibles());
        }
        return Collections.max(availablesPlaces);
    }

    public static List<Parking> getParkingsFromAddresses(List<Parking> parkingsToFilter, String address) {
        List<Parking> parkings = new ArrayList<>();
        for (Parking parking : parkingsToFilter) {
            if (getAddressFromParking(parking).toLowerCase().contains(address.toLowerCase())) {
                parkings.add(parking);
            }
        }
        return parkings;
    }

    public static boolean isOlderThanSevenDays(String date) {
        String[] dateExploded = date.split(DATE_SEPARATOR);
        if (dateExploded.length == 3) {
            Calendar calendar = new GregorianCalendar(Integer.valueOf(dateExploded[2]),
                    Integer.valueOf(dateExploded[1]), Integer.valueOf(dateExploded[0]));
            calendar.add(Calendar.DAY_OF_MONTH, MAX_DAYS_FOR_REFERENTIEL_UPDATE);
            return new Date().after(calendar.getTime());
        } else {
            Log.e("[Utils]", "Mauvaise syntaxe de date utilisée pour l'update du référentiel");
            return false;
        }
    }

    public static String getTodayDateAsString() {
        return new SimpleDateFormat(DATE_FORMAT).format(new Date());
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static String getParkingStatutFromStatutId(GroupeParking parking){
        String statut;
        switch (parking.grpStatut) {
            case "0" : statut = ApiConstants.BUG_API;
                break;
            case "1" : statut = ApiConstants.FERME;
                break;
            case "2" : statut = ApiConstants.ABONNEES;
                break;
            case "5" :
                if(Integer.valueOf(parking.grpDisponible) > 0 ) {
                    statut = ApiConstants.OUVERT;
                } else {
                    statut = ApiConstants.COMPLET;
                }
                break;
            default: statut = ApiConstants.NOT_DEFINE;
        }
        return statut;
    }

}
