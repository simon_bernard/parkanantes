package org.parkanantes.miage.tools.exception;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.parkanantes.miage.R;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Classe pour gérer les messages d'erreurs courants, via les classes d'exceptions customisées
 */
public class ExceptionMessageManager {

    private static final String TAG = ExceptionMessageManager.class.getName();

    private ExceptionMessageManager() {
        // private constructor
    }

    /**
     * Méthode pour générer une classe customisée d'exception, selon l'exception reçue
     * @param exception l'exception à gérer
     * @return une classe d'exception customisée
     */
    public static CustomComplexException manageExceptionMessage(Throwable exception) {
        try {
            if (exception instanceof SocketTimeoutException) {
                return new CustomComplexException(R.string.time_out_exception_message, true, exception);
            } else if (exception instanceof UnknownHostException) {
                return new CustomComplexException(R.string.no_internet_exception_message, true, exception);
            }
        } catch (Exception e) {
            Log.e(TAG, "Erreur dans la gestion du message d'erreur : " + exception.getMessage());
        }
        return new CustomComplexException(0, false, exception);
    }

    /**
     * Méthode permettant d'afficher un message en Toast à partir d'une exception
     * @param error l'exception à gérer
     * @param context le context de l'activité pour accéder aux ressources et faire le Toast
     */
    public static void showException(final Exception error, final Context context) {
        if (error instanceof CustomComplexException) {
            CustomComplexException customComplexException = (CustomComplexException) error;
            String text = "";
            if (customComplexException.isCustom()) {
                text = context.getResources().getString(customComplexException.getMessageHeadingId())
                        + context.getResources().getString(customComplexException.getMessageContentId());
            } else {
                text = context.getResources().getString(customComplexException.getMessageHeadingId())
                        + customComplexException.getMessage();
            }
            Toast.makeText(context, text, Toast.LENGTH_LONG).show();
        } else if (error instanceof CustomSimpleException) {
            CustomSimpleException customSimpleException = (CustomSimpleException) error;
            Toast.makeText(context, context.getResources().getString(customSimpleException.getMessageId()), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
