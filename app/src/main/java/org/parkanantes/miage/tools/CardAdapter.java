package org.parkanantes.miage.tools;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.parkanantes.miage.DetailActivity;
import org.parkanantes.miage.R;
import org.parkanantes.miage.model.database.Parking;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Getter;

/**
 * Classe gérant les cartes à afficher pour chaque parking dans l'activité de liste des parkings
 */
@Getter
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ParkingViewHolder>{

    private List<Parking> mDatas;
    private Context context;

    public static class ParkingViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameParking)
        TextView nomparking;

        @BindView(R.id.addressParking)
        TextView addressParking;

        @BindView(R.id.numberFreeParkingSpace)
        TextView numberFreeParkingSpace;

        @BindView(R.id.numberTotalParkingSpace)
        TextView numberTotalParkingSpace;

        @BindView(R.id.favStarParking)
        ImageView favoriteStar;

        @BindView(R.id.csa_image_cb)
        ImageView cb;

        @BindView(R.id.csa_image_espece)
        ImageView espece;

        @BindView(R.id.csa_image_total)
        ImageView carteTotal;

        @BindView(R.id.csa_image_cheque)
        ImageView cheque;

        @BindView(R.id.cardViewConstraintLayout)
        View constraintLayout;

        @BindView(R.id.cardviewTitle)
        View cardTitle;

        @BindView(R.id.labelStatut)
        TextView labalStatut;

        private View view;

        private String parkingId;

        public ParkingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);

            this.view = view;
            this.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent seePlaceDetailIntent = new Intent(view.getContext(), DetailActivity.class);
                    seePlaceDetailIntent.putExtra("id_parking", parkingId);
                    view.getContext().startActivity(seePlaceDetailIntent);
                }
            });
        }

        @OnClick(R.id.csa_image_cb)
        public void onCbImageClick() {
            Toast.makeText(view.getContext(), ApiConstants.CB_BORNE_SORTIE, Toast.LENGTH_SHORT).show();
        }

        @OnClick(R.id.csa_image_cheque)
        public void onChequeImageClick() {
            Toast.makeText(view.getContext(), ApiConstants.CHEQUE, Toast.LENGTH_SHORT).show();
        }

        @OnClick(R.id.csa_image_espece)
        public void onEspeceImageClick() {
            Toast.makeText(view.getContext(), ApiConstants.ESPECE, Toast.LENGTH_SHORT).show();
        }

        @OnClick(R.id.csa_image_total)
        public void onTotalImageClick() {
            Toast.makeText(view.getContext(), ApiConstants.TOTAL_GR, Toast.LENGTH_SHORT).show();
        }
    }

    public CardAdapter(List<Parking> mDatas, Context context){
        this.mDatas = mDatas;
        this.context = context;
    }

    @Override
    public CardAdapter.ParkingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_summary_adapter, parent, false);
        return new ParkingViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ParkingViewHolder holder, int position) {
        holder.nomparking.setText(mDatas.get(position).getNom());
        holder.addressParking.setText(mDatas.get(position).getAdresse()+ ", " + mDatas.get(position).getCodePostal() + " " + mDatas.get(position).getCommune());
        holder.numberFreeParkingSpace.setText(String.valueOf(mDatas.get(position).getPlacesDisponibles()));
        holder.numberTotalParkingSpace.setText(String.valueOf(mDatas.get(position).getCapaciteVoiture()));
        
        manageMoyenPaiement(mDatas.get(position),holder);
        manageColorAndLabel(mDatas.get(position),holder);

        holder.parkingId = mDatas.get(position).getIdParking();
        if (mDatas.get(position).isFavorite()) {
            holder.favoriteStar.setImageResource(R.drawable.star_on);
        } else {
            holder.favoriteStar.setImageResource(R.drawable.star_off);
        }

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    private void manageMoyenPaiement(Parking parking, ParkingViewHolder holder ) {
        List<String> paymentMeans = Utils.getUniquePaymentMeansOneParking(parking);
        if(paymentMeans.contains(ApiConstants.CB_BORNE_SORTIE)){
            holder.cb.setVisibility(View.VISIBLE);
        }
        if(paymentMeans.contains(ApiConstants.ESPECE)){
            holder.espece.setVisibility(View.VISIBLE);
        }
        if(paymentMeans.contains(ApiConstants.TOTAL_GR)){
            holder.carteTotal.setVisibility(View.VISIBLE);
        }
        if(paymentMeans.contains(ApiConstants.CHEQUE)){
            holder.cheque.setVisibility(View.VISIBLE);
        }
    }

    private void manageColorAndLabel(Parking parking, ParkingViewHolder holder) {
        if(ApiConstants.OUVERT.equals(parking.getParkingStatut())){
            changeColor(holder, parking.getParkingStatut(), R.color.green, R.color.lightGreen);
        }else if(ApiConstants.FERME.equals(parking.getParkingStatut())
                || ApiConstants.COMPLET.equals(parking.getParkingStatut()) ){
            changeColor(holder, parking.getParkingStatut(), R.color.red, R.color.lightRed);
        }else if(ApiConstants.ABONNEES.equals(parking.getParkingStatut())){
            changeColor(holder, parking.getParkingStatut(), R.color.blue, R.color.lightBlue);
        }
    }

    private void changeColor(ParkingViewHolder holder, String type, int colorTitle, int colorBackgorund) {
        holder.labalStatut.setText(type);
        holder.constraintLayout.setBackground(context.getResources().getDrawable(colorBackgorund, null));
        holder.cardTitle.setBackground(context.getResources().getDrawable(colorTitle, null));
    }

}
